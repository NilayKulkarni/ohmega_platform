import os

#Tornado imports
import tornado.httpserver
import tornado.ioloop
from tornado.ioloop import *
import tornado.options
import tornado.web
from tornado.web import *
import tornado.escape
import tornado.template

import py2neo
from py2neo import *

import pymongo
from pymongo import *

from tornado_cors import CorsMixin

from libohmega.genmethods import *
from libohmega.mongomethods import *
from libohmega.neomethods import *
from libohmega.password import *
from libohmega.vars import *

class JoinPageHandler(RequestHandler):
    @asynchronous
    @addslash
    def get(self):
        self.render(
            "templates/join.html",
            title="Join | ohmega",
            host=serverHost)

class JoinEmailHandler(CorsMixin, RequestHandler):
    CORS_ORIGIN = '*'
    CORS_HEADERS = 'Content-Type'
    CORS_METHODS = 'POST'
    @asynchronous
    #@asyncio.coroutine
    def post(self):
        print("REQUEST BODY: " + str(self))
        request_body = tornado.escape.json_decode(self.request.body)
        email_user = request_body['email']
        password = request_body['pass']

        confirmCode = generateConfirmCode()

        storeConfirm = storeEmailConfirm(confirmCode, email_user, password)
        if storeConfirm == True:
            self.write('success')
            self.finish()
        elif storeConfirm == False :
            self.write('failed')
            self.finish()
        elif storeConfirm == "State Active":
            self.write('active')
            self.finish()

class ConfirmationHandler(RequestHandler):
    @addslash
    @asynchronous
    def get(self, code):
        db.alchemists.update({'confirmCode' : code},{"$set":{"state" : True}})
        user = db.alchemists.find_one({'confirmCode' : code})
        password_plain = user['password']
        email_user = user['id']
        createAlchemist(email_user, password_plain)

        self.redirect("/profile/"+email_user, permanent=False)

class ProfileHandler(RequestHandler):
    @addslash
    @asynchronous
    def get(self, username):
        pass

class LoginHandler(RequestHandler):
    @addslash
    @asynchronous
    def get(self):
        try:
            errormessage = self.get_argument("error")
        except:
            errormessage = ""
        self.render("templates/login.html", errormessage = errormessage)
    def post(self):
        self.set_secure.cookie("alchemist", self.get_argument("username"))

settings = {
    "static_path" : os.path.join(os.path.dirname(__file__), "static_files"),
    "cookie_secret" : cookie_secret,
    "login_url" : "/login"
}

if __name__ == '__main__':
    tornado.options.parse_command_line()
    app = tornado.web.Application(
        handlers = [
            (r"/join", JoinPageHandler),
            (r"/join/", JoinPageHandler),
            (r"/confirmemail", JoinEmailHandler),
            (r"/confirmemail/", JoinEmailHandler),
            (r"/key/([a-zA-Z_0-9]+)", ConfirmationHandler),
            (r"/key/([a-zA-Z_0-9]+)/", ConfirmationHandler),
            (r"/profile/([a-zA-Z_0-9.@]+)", ProfileHandler),
            (r"/profile/([a-zA-Z_0-9.@]+)/", ProfileHandler),
            (r"/login", LoginHandler),
            (r"/login/", LoginHandler)],
        **settings
    )
    http_server = tornado.httpserver.HTTPServer(app, xheaders=True)
    #http_server.start(0)
    #http_server.bind(8181)
    http_server.listen(8181)
    tornado.ioloop.IOLoop.instance().start()
