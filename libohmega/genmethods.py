import string
import random
from tornado.gen import coroutine
from email.message import EmailMessage
from tornado_smtp.client import TornadoSMTP
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from libohmega.vars import *

def generateConfirmCode(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

@coroutine
def sendMail(email_user, password, confirmCode):
    client = TornadoSMTP('smtp.mailgun.org')
    yield client.starttls()
    yield client.login('welcome@ohme.ga', 'OhmegaMail2048')
    email_ohmega = "Welcome@ohme.ga"
    #MIME
    msg = MIMEMultipart('alternative')
    msg['Subject'] = "Your Key To ohmega!"
    msg["From"] = email_ohmega
    msg['To'] = email_user
    #Email body
    text = "This is an email to confirm you joining the ohmega family"
    html = """\
        <html>
        <head></head>
        <body>
        <p>Hey,<br>
        Here is the confirmation <a href=http://""" + serverHost+ ":8181" + """/key/""" + confirmCode + """>link</a></p></body></html>"""
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')
    msg.attach(part1)
    msg.attach(part2)
    yield client.send_message(msg)
    yield client.quit()
