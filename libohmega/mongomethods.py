import pymongo
from pymongo import *
from libohmega.vars import *
from libohmega.genmethods import sendMail

#Setup MongoDB
dbclient = MongoClient(host="ds011840.mlab.com", port=11840)
dbclient.ohmega.authenticate('ohmegaUser', 'OhmegaApp2048')
db = dbclient.ohmega

def storeEmailConfirm(confirmCode, email_user, password):
    existingConfirm = db.alchemists.find_one({
        'id' : email_user
    })
    print(existingConfirm)
    if existingConfirm == None:
        db.alchemists.insert({
            'id' : email_user,
            'state' : False,
            'password' : password,
            'confirmCode' : confirmCode
        })
        sendMail(email_user, password, confirmCode)
        return True
    else:
        if existingConfirm['state'] == True:
            return 'State Active'
        return False
