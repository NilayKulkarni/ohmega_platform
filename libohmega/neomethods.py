from libohmega import hashpasswd
from libohmega.vars import *
from py2neo import Node, Relationship, watch, authenticate, Graph

#Setup Neo4j
neoGraph = Graph("http://neo4j:Neo4Ohmega@" + serverHost + ":7474/db/data/")
print(neoGraph.neo4j_version)

def createAlchemist(email, password):
    password_encrypted = hashpasswd(password)
    alchemist = Node("Alchemist", email=email, password=password_encrypted, username=email)
    neoGraph.create(alchemist)
