import bcrypt
import hashlib
import base64
import string

def hashpasswd(plain_password):
    return bcrypt.hashpw(plain_password.encode('utf-8'), bcrypt.gensalt(13))

def checkpasswd(plain_password, hash_user):
    if bcrypt.hashpw(plain_password.encode('utf-8'), bcrypt.gensalt(13)) == hash_user:
        return True
    else:
        return False
