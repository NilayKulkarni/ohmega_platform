$('#btn-join').click(function(){
    var emailid = $('#join_email').val();
    var password = $('#password').val();
    if(emailid != "" && password.length >= 8){
        var epass = JSON.stringify({
                email:emailid,
                pass:password});
        $.ajax({
            type : "POST",
            url : "http://" + serverHost + ":8181/confirmemail/",
            data : epass,
            contentType : "application/json; charset=utf-8",
            dataType : 'json',
            success : function(msg, status, jqXHR){
                if(msg == "success"){
                    Materialize.toast("We Have Mailed You The Key To ohmega :)", 4000);
                } else if (msg == "failed"){
                    Materialize.toast("You Already Have The Key To ohmega In Your Mailbox ;)", 4000);
                } else if (msg == 'active'){
                    Materialize.toast("You Are Already One Of Us :)", 4000);
                }
            },
            dataType : "text",
            async : true,
        });
    } else{
        if(emailid == ""){
            Materialize.toast("Email-id Cannot Be Blank!");
        } else if (password.length <=7){
            Materialize.toast("Password Needs To Have More Than 8 Characters");
        }
    }
});
$(document).keypress(function(k){
    if(k.which == 13){
        $("#btn-join").click();
    }
})